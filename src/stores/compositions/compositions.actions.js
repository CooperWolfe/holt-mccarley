import Modules from '../app.modules'
import firebase from '../../services/firebase'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const SET_COMPOSITIONS = 'SET_COMPOSITIONS'
export const SET_COMPOSITION_TYPE_IDS = 'SET_COMPOSITION_TYPE_IDS'
export const UPDATE_COMPOSITION_TYPE_ID = 'UPDATE_COMPOSITION_TYPE_ID'
export const ADD_COMPOSITION = 'ADD_COMPOSITION'
export const REMOVE_COMPOSITION_TYPE_ID = 'REMOVE_COMPOSITION_TYPE_ID'
export const REMOVE_COMPOSITION_FILE = 'REMOVE_COMPOSITION_FILE'
export const REMOVE_COMPOSITION = 'REMOVE_COMPOSITION'
export const UPDATE_COMPOSITION = 'UPDATE_COMPOSITION'
export const ADD_COMPOSITION_FILE = 'ADD_COMPOSITION_FILE'

// Synchronous
export const request = () => ({
  module: Modules.COMPOSITIONS,
  type: REQUEST
})
export const receive = () => ({
  module: Modules.COMPOSITIONS,
  type: RECEIVE
})
export const error = err => ({
  module: Modules.COMPOSITIONS,
  type: ERROR,
  err
})
export const setCompositions = (compositions, compositionTypeId) => ({
  module: Modules.COMPOSITIONS,
  type: SET_COMPOSITIONS,
  compositions, compositionTypeId
})
export const setCompositionTypeIds = compositionTypeIds => ({
  module: Modules.COMPOSITIONS,
  type: SET_COMPOSITION_TYPE_IDS,
  compositionTypeIds
})
export const updateCompositionTypeId = (compositionTypeId, previousCompositionTypeId) => ({
  module: Modules.COMPOSITIONS,
  type: UPDATE_COMPOSITION_TYPE_ID,
  previousCompositionTypeId, compositionTypeId
})
export const addComposition = (composition, compositionTypeId) => ({
  module: Modules.COMPOSITIONS,
  type: ADD_COMPOSITION,
  composition, compositionTypeId
})
export const removeCompositionTypeId = compositionTypeId => ({
  module: Modules.COMPOSITIONS,
  type: REMOVE_COMPOSITION_TYPE_ID,
  compositionTypeId
})
export const removeCompositionFile = (compositionTypeId, compositionIndex, fileIndex) => ({
  module: Modules.COMPOSITIONS,
  type: REMOVE_COMPOSITION_FILE,
  compositionTypeId, compositionIndex, fileIndex
})
export const removeComposition = (compositionTypeId, compositionIndex) => ({
  module: Modules.COMPOSITIONS,
  type: REMOVE_COMPOSITION,
  compositionTypeId, compositionIndex
})
export const updateComposition = (compositionTypeId, compositionIndex, composition) => ({
  module: Modules.COMPOSITIONS,
  type: UPDATE_COMPOSITION,
  compositionTypeId, compositionIndex, composition
})
export const addCompositionFile = (compositionTypeId, compositionIndex, file) => ({
  module: Modules.COMPOSITIONS,
  type: ADD_COMPOSITION_FILE,
  compositionTypeId, compositionIndex, file
})

// Asynchronous
const collectionRef = firebase.firestore().collection('compositions')
export const fetchCompositions = compositionTypeId => dispatch => {
  dispatch(request())
  return collectionRef.doc(compositionTypeId).get()
    .then(snapshot => snapshot.get('compositions'))
    .then(compositions => dispatch(setCompositions(compositions, compositionTypeId)))
    .then(() => dispatch(receive()))
    .catch(err => dispatch(error(err)))
}
export const storeComposition = (composition, compositionTypeId) => (dispatch, getState) => {
  const previousCompositions = getState().compositions.compositions
  dispatch(request())
  dispatch(addComposition(composition, compositionTypeId))
  return collectionRef.doc(compositionTypeId).update({ compositions: firebase.firestore.FieldValue.arrayUnion(composition) })
    .then(() => dispatch(receive()))
    .catch(err => {
      dispatch(setCompositions(previousCompositions))
      return dispatch(error(err))
    })
}
export const deleteCompositionFile = (compositionTypeId, compositionIndex, fileIndex) => (dispatch, getState) => {
  const previousCompositions = getState().compositions.compositions
  dispatch(request())
  dispatch(removeCompositionFile(compositionTypeId, compositionIndex, fileIndex))
  return collectionRef.doc(compositionTypeId).update({ compositions: [
      ...previousCompositions[compositionTypeId].slice(0, compositionIndex),
      {
        ...previousCompositions[compositionTypeId][compositionIndex],
        files: [
          ...previousCompositions[compositionTypeId][compositionIndex].files.slice(0, fileIndex),
          ...previousCompositions[compositionTypeId][compositionIndex].files.slice(fileIndex + 1)
        ]
      },
      ...previousCompositions[compositionTypeId].slice(compositionIndex + 1)
    ] })
    .then(() => {
      return firebase.storage().refFromURL(previousCompositions[compositionTypeId][compositionIndex].files[fileIndex].url).delete()
        .then(() => dispatch(receive()))
        .catch(err => { throw err })
    })
    .catch(err => {
      dispatch(setCompositions(previousCompositions))
      return dispatch(error(err))
    })
}
export const postComposition = (compositionTypeId, compositionIndex, composition) => (dispatch, getState) => {
  const previousCompositions = getState().compositions.compositions
  dispatch(request())
  dispatch(updateComposition(compositionTypeId, compositionIndex, composition))
  return collectionRef.doc(compositionTypeId).update({ compositions: [
      ...previousCompositions[compositionTypeId].slice(0, compositionIndex),
      {
        ...previousCompositions[compositionTypeId][compositionIndex],
        ...composition
      },
      ...previousCompositions[compositionTypeId].slice(compositionIndex + 1)
    ] })
    .then(() => dispatch(receive()))
    .catch(err => {
      dispatch(setCompositions(previousCompositions))
      return dispatch(error(err))
    })
}
export const storeCompositionFile = (compositionTypeId, compositionIndex, file) => (dispatch, getState) => {
  const previousCompositions = getState().compositions.compositions
  dispatch(request())
  return firebase.storage().ref(`compositions/${compositionTypeId}/${file.file.name}`).put(file.file)
    .then(snapshot => {
      return snapshot.ref.getDownloadURL()
        .then(url => {
          file = { description: file.description, url }
          dispatch(addCompositionFile(compositionTypeId, compositionIndex, file))
          return collectionRef.doc(compositionTypeId).update({ compositions: [
              ...previousCompositions[compositionTypeId].slice(0, compositionIndex),
              {
                ...previousCompositions[compositionTypeId][compositionIndex],
                files: [
                  ...previousCompositions[compositionTypeId][compositionIndex].files,
                  file
                ]
              },
              ...previousCompositions[compositionTypeId].slice(compositionIndex + 1)
            ] })
            .then(() => dispatch(receive()))
            .catch(err => { throw err })
        })
        .catch(err => { throw err })
    })
    .catch(err => {
      dispatch(setCompositions(previousCompositions))
      return dispatch(error(err))
    })
}
export const deleteComposition = (compositionTypeId, compositionIndex) => (dispatch, getState) => {
  dispatch(request())
  const previousCompositions = getState().compositions.compositions
  dispatch(removeComposition(compositionTypeId, compositionIndex))
  return collectionRef.doc(compositionTypeId).update({ compositions: [
      ...previousCompositions[compositionTypeId].slice(0, compositionIndex),
      ...previousCompositions[compositionTypeId].slice(compositionIndex + 1)
    ] })
    .then(() => {
      previousCompositions[compositionTypeId][compositionIndex].files.forEach(file => {
        firebase.storage().refFromURL(file.url).delete()
          .catch(err => { throw err })
      })
    })
    .then(() => dispatch(receive()))
    .catch(err => {
      dispatch(setCompositions(previousCompositions))
      dispatch(error(err))
    })
}
