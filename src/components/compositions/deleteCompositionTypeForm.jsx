import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { deleteCompositionType } from '../../stores/composition-types/compositionTypes.actions'
import logService from '../../services/log'
import history from '../../services/history'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  onDeleteCompositionType: compositionTypeId => dispatch(deleteCompositionType(compositionTypeId))
})

const propTypes = {
  compositionTypeId: PropTypes.string.isRequired
}

class DeleteCompositionTypeForm extends Component {
  state = {
    deleteValidationText: ''
  }

  render() {
    const { className } = this.props
    const { deleteValidationText } = this.state

    return (
      <form onSubmit={this.handleSubmit} className={className}>
        <div className='input-group'>
          <input type='text'
                 className='form-control'
                 value={deleteValidationText}
                 onChange={this.handleChange}
                 placeholder='Enter the ID to delete entire section' />
          <div className='input-group-append'>
            <button className='btn btn-outline-danger' type='submit'>Delete</button>
          </div>
        </div>
      </form>
    )
  }

  handleChange = e => {
    this.setState({ deleteValidationText: e.target.value })
  }
  handleSubmit = e => {
    e.preventDefault()
    const { deleteValidationText } = this.state
    const { onDeleteCompositionType, compositionTypeId } = this.props

    if (deleteValidationText === compositionTypeId)
      onDeleteCompositionType(compositionTypeId).then(() => history.push('/admin/compositions'))
    else logService.alertError('Incorrect ID')
  }
}

DeleteCompositionTypeForm.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(DeleteCompositionTypeForm)
