import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  linkStyle: PropTypes.object
}
const defaultProps = {
  style: {},
  className: '',
  linkStyle: {}
}

class NotFound extends Component {
  render() {
    const { style, linkStyle, className } = this.props

    return (
      <div className={`text-center ${className}`} style={style}>
        <div className='w-100 h1'>Whoops!</div>
        <p>We couldn't find your page. Why don't you go back <Link to='/' style={linkStyle}>home</Link>?</p>
      </div>
    )
  }
}

NotFound.propTypes = propTypes
NotFound.defaultProps = defaultProps

export default NotFound
