import * as Actions from './bio.actions'
import Modules from '../app.modules'

export const initialState = {
  loading: false,
  error: null,
  bio: ''
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function setBio(state, { bio }) {
  return { ...state, bio }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.BIO) throw new Error(`Wrong module. Expected: ${Modules.BIO}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.SET_BIO: return setBio(state, action)
  default: return state
  }
}
