import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Form from '../../helpers/form'
import PropTypes from 'prop-types'
import Joi from 'joi-browser'
import {
  deleteCompositionFile,
  postComposition,
  storeCompositionFile
} from '../../stores/compositions/compositions.actions'
import uuid from 'uuid/v4'
import Loader from '../loader'
import logService from '../../services/log'

const mapStateToProps = state => ({
  compositions: state.compositions.compositions,
  loading: state.compositions.loading
})
const mapDispatchToProps = dispatch => ({
  onDeleteFile: (compositionTypeId, compositionIndex, fileIndex) => dispatch(deleteCompositionFile(compositionTypeId, compositionIndex, fileIndex)),
  onUpdateComposition: (compositionTypeId, compositionIndex, composition) => dispatch(postComposition(compositionTypeId, compositionIndex, composition)),
  onUploadFile: (compositionTypeId, compositionIndex, file) => dispatch(storeCompositionFile(compositionTypeId, compositionIndex, file))
})

const propTypes = {
  compositionTypeId: PropTypes.string.isRequired,
  compositionIndex: PropTypes.number.isRequired
}

class UpdateCompositionForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      title: this.props.compositions[this.props.compositionTypeId][this.props.compositionIndex].title,
      year: this.props.compositions[this.props.compositionTypeId][this.props.compositionIndex].year,
      description: this.props.compositions[this.props.compositionTypeId][this.props.compositionIndex].description
    },
    fileDescription: ''
  }
  schema = {
    title: Joi.string().required().error(() => ({ message: 'Title cannot be empty' })),
    year: Joi.number().required().error(() => ({ message: 'Year cannot be empty' })),
    description: Joi.string().optional()
  }

  render() {
    const { compositionTypeId, className, compositions, compositionIndex, loading } = this.props
    const { fileDescription } = this.state
    const files = compositions[compositionTypeId][compositionIndex].files

    return (
      <Fragment>
        <div className='text-left'>
          <Link className='c-accent' to={`/admin/compositions/${compositionTypeId}`}>
            <i className='fa fa-angle-left' /> Back
          </Link>
        </div>
        <h2 className='c-accent'>Update Composition</h2>
        <form onSubmit={this.handleSubmit} className={className}>
          { this.renderField('Title', 'title', 'text', 'text-center') }
          { this.renderField('Year', 'year', 'number', 'text-center pl-4') }
          { this.renderTextArea('Description', 'description', 2) }
          { this.renderButton('submit', 'outline-success', loading ? <Loader /> : 'Save') }
        </form>

        <hr/>
        <h2 className='c-accent'>Upload Files</h2>
        <div className='col-md-8 m-auto'>
          <ul className='list-group'>
            { files.map((file, i) => (
              <li key={i} className='list-group-item text-left bg-light'>
                <a className='c-accent' key={i} href={file.url}>{ file.description } <i className='fa fa-external-link'/></a>
                <i onClick={() => this.handleDelete(i)} className='fa fa-remove float-right c-primary o-hover-50'/>
              </li>
            )) }
            <li className='list-group-item text-left bg-light'>
              <div className='col-8 d-inline-block'>
                <input className='form-control' type='text' placeholder='File description' value={fileDescription} onChange={this.handleFileDescriptionChange}/>
              </div>
              <div className='col-4 d-inline-block text-center'>
                <label htmlFor='file' className='btn btn-outline-success'>{ loading ? <Loader /> : 'Choose File' }</label>
                <input type='file' onChange={this.handleFileChange} id='file' hidden />
              </div>
            </li>
          </ul>
        </div>
      </Fragment>
    )
  }

  doSubmit = () => {
    const { compositionTypeId, compositionIndex, onUpdateComposition } = this.props
    const { data } = this.state
    onUpdateComposition(compositionTypeId, compositionIndex, data).then(() => {
      logService.alert('Successfully updated composition')
    })
  }
  handleDelete = fileIndex => {
    const { compositionTypeId, compositionIndex, onDeleteFile } = this.props
    onDeleteFile(compositionTypeId, compositionIndex, fileIndex)
  }
  handleFileChange = e => {
    let file = e.target.files[0]
    const { onUploadFile, compositionTypeId, compositionIndex } = this.props
    const { fileDescription } = this.state

    if (!file) return
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1)
    file = new File([file], `${uuid()}.${extension}`, { type: file.type })

    onUploadFile(compositionTypeId, compositionIndex, { file, description: fileDescription })
      .then(() => {
        this.setState({ fileDescription: '' })
      })
  }
  handleFileDescriptionChange = e => {
    this.setState({
      fileDescription: e.target.value
    })
  }
}

UpdateCompositionForm.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCompositionForm)
