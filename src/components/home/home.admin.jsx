import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../../../node_modules/react-responsive-carousel/lib/styles/carousel.min.css'
import { deleteGalleryImage, fetchGallery, storeGalleryImage } from '../../stores/gallery/gallery.actions'
import { Link } from 'react-router-dom'
import Image from '../image'
import uuid from 'uuid/v4'
import ReactMde from 'react-mde'
import '../../../node_modules/react-mde/lib/styles/css/react-mde-all.css'
import { parse } from 'markdown'
import { fetchBio, storeBio } from '../../stores/bio/bio.actions'
import logService from '../../services/log'
import Loader from '../loader'

const mapStateToProps = state => ({
  imageUrls: state.gallery.gallery,
  bio: state.bio.bio,
  loadingBio: state.bio.loading
})
const mapDispatchToProps = dispatch => ({
  onFetchGallery: () => dispatch(fetchGallery()),
  onFetchBio: () => dispatch(fetchBio()),
  onDeletePhoto: index => dispatch(deleteGalleryImage(index)),
  onUploadPhoto: blob => dispatch(storeGalleryImage(blob)),
  onStoreBio: bio => dispatch(storeBio(bio))
})

class HomeAdmin extends Component {
  state = {
    bio: '',
    fetchedBio: false
  }

  static getDerivedStateFromProps(props, state) {
    if (state.fetchedBio || !props.bio) return null
    return {
      bio: props.bio,
      fetchedBio: true
    }
  }

  componentDidMount() {
    const { onFetchGallery, onFetchBio } = this.props
    onFetchGallery()
    onFetchBio()
  }

  render() {
    const { imageUrls, loadingBio } = this.props
    const { bio, fetchedBio } = this.state

    return (
      <React.Fragment>
        <div className='gallery'>
          <h1 className='text-center c-accent'>Gallery</h1>
          <Link to='/' className='o-50 text-center d-block c-light'>preview <i className='fa fa-external-link'/></Link>
          { imageUrls.map((url, i) => (
            <Image key={i} imageSrc={url} style={{ height: 250 }}
                   objectFit='contain'
                   className='col-lg-2 col-md-3 col-4 p-2 d-inline-block'
                   imageAlt="Holt McCarley's gallery">
              <button onClick={() => this.handleDelete(i)} className='btn-lg w-100 bg-danger c-primary' style={{ marginTop: 101, border: 'none' }}><i className='fa fa-trash'/></button>
            </Image>
          )) }
          <div className='position-relative col-lg-2 col-md-3 col-4 p-2 d-inline-block' style={{ height: 250 }}>
            <div className='position-absolute w-100 h-100'>
              <input onChange={this.handleUploadPhoto} id='upload' className='d-none' type='file' accept='image/*' />
              <label htmlFor='upload' className='btn btn-outline-success w-100 p-2' style={{ margin: '101px .5rem' }}>
                Upload
              </label>
            </div>
          </div>
        </div>
        <div className='bio'>
          <h1 className='text-center c-accent'>Bio</h1>
          <Link to='/' className='o-50 text-center d-block c-light'>preview <i className='fa fa-external-link'/></Link>
          { fetchedBio && <React.Fragment>
            <ReactMde onChange={bio => this.setState({ bio })}
                    className='mt-3'
                    value={bio}
                    generateMarkdownPreview={md => Promise.resolve(parse(md))} buttonContentOptions={{
                      iconProvider: name => <i className={`fa fa-${name}`} />,
                    }} />
            <div className='text-center mt-2'>
              <button onClick={this.handleStoreBio} className='btn btn-success'>
                { loadingBio ? <Loader/> : 'Save' }
              </button>
            </div>
          </React.Fragment> }
        </div>
      </React.Fragment>
    )
  }

  handleDelete = index => {
    const { onDeletePhoto } = this.props
    onDeletePhoto(index)
  }
  handleUploadPhoto = e => {
    let file = e.target.files[0]
    const { onUploadPhoto } = this.props

    if (!file) return
    const extension = file.name.substr(file.name.lastIndexOf('.') + 1)
    file = new File([file], `${uuid()}.${extension}`, { type: file.type })

    onUploadPhoto(file)
  }
  handleStoreBio = () => {
    const { bio } = this.state
    const { onStoreBio } = this.props
    onStoreBio(bio).then(() => logService.alert('Successfully submitted bio'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeAdmin)
