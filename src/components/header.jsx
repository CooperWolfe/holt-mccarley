import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  imgStyle: PropTypes.object,
  textStyle: PropTypes.object
}
const defaultProps = {
  alt: '',
  imgStyle: {},
  textStyle: {}
}

class Header extends Component {
  render() {
    const { src, alt, children } = this.props
    let { imgStyle, textStyle } = this.props

    imgStyle = {
      objectFit: 'cover',
      objectPosition: 'center',
      position: 'absolute',
      top: 0, bottom: 0, left: 0, right: 0,
      width: '100%',
      height: 500,
      ...imgStyle
    }
    textStyle = {
      left: 0, right: 0, top: 0, bottom: 0,
      padding: '150px 10%',
      position: 'absolute',
      fontSize: '4rem',
      ...textStyle
    }

    return (
      <div className="header position-relative" style={{ height: imgStyle.height }}>
        <img src={src} alt={alt} style={imgStyle} />
        <div style={textStyle}>{ children }</div>
      </div>
    )
  }
}

Header.propTypes = propTypes
Header.defaultProps = defaultProps

export default Header
