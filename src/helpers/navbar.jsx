import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { NavbarToggler, Collapse, DropdownToggle, DropdownMenu, Dropdown } from 'reactstrap'

const propTypes = {
  style: PropTypes.object,
  breakpoint: PropTypes.string
}
const defaultState = {
  left: [],
  right: [],
  linkOpen: [],
  open: false
}

class Navbar extends Component {
  state = { ...defaultState }

  renderBrand = () => 'Update renderBrand()'
  renderLinks = (links, className) => {
    className = className ? `navbar-nav ${className}` : 'navbar-nav'

    return (
      <ul className={className}>
        { links.map((link, i) => {
          if (link.links) return this.renderDropdownLink(link, i)
          if (link.url) return this.renderLink(link, i)
          return <li key={i}>Links must be of the format {'{'} text: string, [url/links]: [string/array] {'}'}</li>
        }) }
      </ul>
    )
  }
  renderDropdownLink = (link, i) => {
    const { linkOpen } = this.state

    return (
      <Dropdown nav key={i} className='m-auto' isOpen={linkOpen[i]} toggle={() => this.toggleLink(i)}>
        <DropdownToggle nav caret>{ link.text }</DropdownToggle>
        <DropdownMenu onClick={() => this.toggleLink(i)}>
          { link.links.map((link, i) => (
            <NavLink exact={link.exact} key={i} className='dropdown-item' to={link.url}>{ link.text }</NavLink>
          )) }
        </DropdownMenu>
      </Dropdown>
    )
  }
  renderLink = (link, i) => {
    return (
      <li key={i} className='nav-item m-auto'>
        <NavLink exact={link.exact} to={link.url} className='nav-link'>{ link.text }</NavLink>
      </li>
    )
  }
  render() {
    const { style, breakpoint } = this.props
    const { open, left, right } = this.state

    let className
    switch (breakpoint) {
    case 'sm': className = 'navbar navbar-expand-sm'; break
    case 'md': className = 'navbar navbar-expand-md'; break
    case 'lg': className = 'navbar navbar-expand-lg'; break
    case 'xl': className = 'navbar navbar-expand-xl'; break
    default: className = 'navbar navbar-expand'
    }

    return (
      <nav className={className} style={{ ...style }}>
        <div className='container'>
          <NavLink className='navbar-brand' to='/' exact>{ this.renderBrand() }</NavLink>
          <NavbarToggler onClick={this.toggle}><i className='fa fa-bars'/></NavbarToggler>
          <Collapse isOpen={open} navbar>
            { this.renderLeft ? this.renderLeft() : this.renderLinks(left, 'mr-auto') }
            { this.renderRight ? this.renderRight() : this.renderLinks(right) }
          </Collapse>
        </div>
      </nav>
    )
  }

  toggle = () => {
    const { open } = this.state
    this.setState({ open: !open })
  }
  toggleLink = i => {
    let { linkOpen: old } = this.state

    const linkOpen = Array(old.length)
    linkOpen.fill(false)
    linkOpen[i] = !old[i]

    this.setState({ linkOpen })
  }
}

Navbar.propTypes = propTypes
Navbar.defaultState = defaultState

export default Navbar
