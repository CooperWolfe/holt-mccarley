import Modules from './app.modules'
import * as Auth from './auth/auth.reducer'
import * as Bio from './bio/bio.reducer'
import * as Compositions from './compositions/compositions.reducer'
import * as CompositionTypes from './composition-types/compositionTypes.reducer'
import * as Contact from './contact/contact.reducer'
import * as Gallery from './gallery/gallery.reducer'

const initialState = {
  auth: Auth.initialState,
  bio: Bio.initialState,
  compositions: Compositions.initialState,
  compositionTypes: CompositionTypes.initialState,
  contact: Contact.initialState,
  gallery: Gallery.initialState,
}

export default function reducer(state = initialState, action) {
  switch (action.module) {
  case Modules.AUTH: return { ...state, auth: Auth.reducer(state.auth, action) }
  case Modules.BIO: return { ...state, bio: Bio.reducer(state.bio, action) }
  case Modules.COMPOSITIONS: return { ...state, compositions: Compositions.reducer(state.compositions, action) }
  case Modules.COMPOSITION_TYPES: return { ...state, compositionTypes: CompositionTypes.reducer(state.compositionTypes, action) }
  case Modules.CONTACT: return { ...state, contact: Contact.reducer(state.contact, action) }
  case Modules.GALLERY: return { ...state, gallery: Gallery.reducer(state.gallery, action) }
  default: return state
  }
}
