import React, { Component } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import NotFound from './notFound'
import Home from './home/home'
import HomeAdmin from './home/home.admin'
import Connect from './connect/connect'
import ConnectAdmin from './connect/connect.admin'
import Compositions from './compositions/compositions'
import CompositionsAdmin from './compositions/compositions.admin'
import Login from './login/login'
import Logout from './logout'

class Content extends Component {
  render() {
    return (
      <div className='container p-3'>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/connect' component={Connect} />
          <Route path='/compositions/:compositionTypeId' component={Compositions} />
          <Route path='/login' component={Login} />
          <Route path='/logout' component={Logout} />

          <Route exact path='/admin' component={HomeAdmin} />
          <Route path='/admin/compositions/:compositionTypeId/:compositionIndex' component={CompositionsAdmin} />
          <Route path='/admin/compositions/:compositionTypeId' component={CompositionsAdmin} />
          <Route path='/admin/compositions' component={CompositionsAdmin} />
          <Route path='/admin/connect' component={ConnectAdmin} />

          <Route path='/not-found' render={() => <NotFound className='c-accent' linkStyle={{ color: 'var(--light)' }}/>}/>
          <Redirect from='/' to='/not-found' />
        </Switch>
      </div>
    )
  }
}

export default Content
