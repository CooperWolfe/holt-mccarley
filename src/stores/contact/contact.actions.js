import Modules from '../app.modules'
import firebase from '../../services/firebase'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const SET_CONTACT = 'SET_CONTACT'

// Synchronous
export const request = () => ({
  module: Modules.CONTACT,
  type: REQUEST
})
export const receive = () => ({
  module: Modules.CONTACT,
  type: RECEIVE
})
export const error = err => ({
  module: Modules.CONTACT,
  type: ERROR,
  err
})
export const setContact = contact => ({
  module: Modules.CONTACT,
  type: SET_CONTACT,
  contact
})

// Asynchronous
const docRef = firebase.firestore().collection('site-data').doc('contact-info')
export const fetchContact = () => dispatch => {
  dispatch(request())
  return docRef.get()
    .then(snapshot => snapshot.data())
    .then(contact => dispatch(setContact(contact)))
    .then(() => dispatch(receive()))
    .catch(err => dispatch(error(err)))
}
export const storeContact = contactInfo => (dispatch, getState) => {
  dispatch(request())
  const previousState = getState().contact.contact
  dispatch(setContact(contactInfo))
  return docRef.update(contactInfo)
    .then(() => dispatch(receive()))
    .catch(err => {
      dispatch(setContact(previousState))
      return dispatch(error(err))
    })
}
