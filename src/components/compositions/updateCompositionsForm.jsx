import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import UpdateCompositionTypeForm from './updateCompositionTypeForm'
import { Link } from 'react-router-dom'
import AddCompositionForm from './addCompositionForm'
import UpdateCompositionForm from './updateCompositionForm'
import DeleteCompositionTypeForm from './deleteCompositionTypeForm.jsx'
import CompositionCard from './compositionCard'
import { deleteComposition } from '../../stores/compositions/compositions.actions'
import history from '../../services/history'

const mapStateToProps = state => ({
  compositions: state.compositions.compositions
})
const mapDispatchToProps = dispatch => ({
  onDeleteComposition: (compositionTypeId, compositionIndex) => dispatch(deleteComposition(compositionTypeId, compositionIndex))
})

const propTypes = {
  compositionTypeId: PropTypes.string.isRequired,
  compositionIndex: PropTypes.string
}

class UpdateCompositionsForm extends Component {
  render() {
    const { compositionTypeId, compositionIndex, compositions } = this.props

    return (
      <Fragment>
        <UpdateCompositionTypeForm className='col-md-6 m-auto' compositionTypeId={compositionTypeId} />
        <hr/>
        { +compositionIndex >= 0 && <UpdateCompositionForm compositionTypeId={compositionTypeId}
                                                           compositionIndex={+compositionIndex}
                                                           className='col-md-6 m-auto' /> }
        { compositionIndex === undefined && (
          <div className='row'>
            { compositions[compositionTypeId] && compositions[compositionTypeId].map((composition, i) => (
              <div key={i}
                   onClick={() => history.push(`/admin/compositions/${compositionTypeId}/${i}`)}
                   className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 text-left p-2'
                   style={{ cursor: 'pointer' }}>
                <CompositionCard composition={composition} onDelete={e => this.handleDelete(e, i)}/>
              </div>
            )) }
            <Link to={`/admin/compositions/${compositionTypeId}/add`}
                  className='col-12 col-sm-6 col-md-4 col-lg-3 mt-3 p-2'>
              <div className='card bg-accent'>
                <div className='card-body c-light'>
                  <h1 className='card-title mb-0'><i className='fa fa-plus'/></h1>
                </div>
              </div>
            </Link>
          </div>
        ) }
        { compositionIndex === 'add' && <AddCompositionForm className='col-md-6 text-center m-auto'
                                                            compositionTypeId={compositionTypeId} /> }
        { compositionIndex === undefined && <Fragment>
          <hr/>
          <DeleteCompositionTypeForm compositionTypeId={compositionTypeId} className='col-md-6 m-auto' />
        </Fragment> }
      </Fragment>
    )
  }

  handleDelete = (e, compositionIndex) => {
    e.preventDefault()
    const { compositionTypeId, onDeleteComposition } = this.props
    onDeleteComposition(compositionTypeId, compositionIndex)
  }
}

UpdateCompositionsForm.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCompositionsForm)
