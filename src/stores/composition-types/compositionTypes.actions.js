import Modules from '../app.modules'
import firebase from '../../services/firebase'
import {
  removeCompositionTypeId,
  setCompositions,
  setCompositionTypeIds,
  updateCompositionTypeId
} from '../compositions/compositions.actions'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const SET_COMPOSITION_TYPES = 'SET_COMPOSITION_TYPES'
export const SELECT_COMPOSITION_TYPE = 'SELECT_COMPOSITION_TYPE'
export const DESELECT_COMPOSITION_TYPE = 'DESELECT_COMPOSITION_TYPE'
export const ADD_COMPOSITION_TYPE = 'ADD_COMPOSITION_TYPE'
export const UPDATE_COMPOSITION_TYPE = 'UPDATE_COMPOSITION_TYPE'
export const REMOVE_COMPOSITION_TYPE = 'REMOVE_COMPOSITION_TYPE'

// Synchronous
export const request = () => ({ module: Modules.COMPOSITION_TYPES, type: REQUEST })
export const receive = () => ({ module: Modules.COMPOSITION_TYPES, type: RECEIVE })
export const error = err => ({ module: Modules.COMPOSITION_TYPES, type: ERROR, err })
export const setCompositionTypes = compositionTypes => ({ module: Modules.COMPOSITION_TYPES, type: SET_COMPOSITION_TYPES, compositionTypes })
export const selectCompositionType = id => ({
  module: Modules.COMPOSITION_TYPES,
  type: SELECT_COMPOSITION_TYPE,
  id: typeof id === 'string' ? id : null,
  compositionType: id
})
export const deselectCompositionType = () => ({ module: Modules.COMPOSITION_TYPES, type: DESELECT_COMPOSITION_TYPE })
export const addCompositionType = compositionType => ({ module: Modules.COMPOSITION_TYPES, type: ADD_COMPOSITION_TYPE, compositionType })
export const updateCompositionType = (compositionType, previousCompositionTypeId) => ({
  module: Modules.COMPOSITION_TYPES,
  type: UPDATE_COMPOSITION_TYPE,
  compositionType, previousCompositionTypeId
})
export const removeCompositionType = id => ({ module: Modules.COMPOSITION_TYPES, type: REMOVE_COMPOSITION_TYPE, id })

// Asynchronous
const docRef = firebase.firestore().collection('site-data').doc('composition-types')
const compositionsCollectionRef = firebase.firestore().collection('compositions')
export const fetchCompositionTypes = () => dispatch => {
  dispatch(request())
  return docRef.get()
    .then(snapshot => snapshot.data())
    .then(compositionTypes => dispatch(setCompositionTypes(compositionTypes)) && compositionTypes)
    .then(compositionTypes => dispatch(setCompositionTypeIds(Object.keys(compositionTypes))) && compositionTypes)
    .then(compositionTypes => dispatch(receive()) && compositionTypes)
    .catch(err => dispatch(error(err)))
}
export const storeCompositionType = compositionType => (dispatch, getState) => {
  const compositionTypeId = Object.keys(compositionType)[0]
  const previousCompositionTypes = getState().compositionTypes.compositionTypes
  if (Object.keys(previousCompositionTypes).includes(compositionTypeId) && previousCompositionTypes[compositionTypeId] !== null)
    return Promise.reject(new Error('There is already a composition type with this ID'))

  dispatch(addCompositionType(compositionType))
  dispatch(request())
  return docRef.update(compositionType)
    .then(() => {
      return compositionsCollectionRef.doc(compositionTypeId).set({ compositions: [] })
        .then(() => dispatch(receive()))
        .catch(err => { throw err })
    })
    .catch(err => {
      dispatch(setCompositionTypes(previousCompositionTypes))
      return dispatch(error(err))
    })
}
export const postCompositionType = (compositionType, previousCompositionTypeId) => (dispatch, getState) => {
  const compositionTypeId = Object.keys(compositionType)[0]
  const previousCompositionTypes = getState().compositionTypes.compositionTypes
  const previousCompositions = getState().compositions.compositions
  if (compositionTypeId !== previousCompositionTypeId && Object.keys(previousCompositionTypes).includes(compositionTypeId) && previousCompositionTypes[compositionTypeId] !== null)
    return Promise.reject(new Error('There is already a composition type with this ID'))

  dispatch(updateCompositionType(compositionType, previousCompositionTypeId))
  dispatch(updateCompositionTypeId(compositionTypeId, previousCompositionTypeId))

  dispatch(request())
  return compositionTypeId === previousCompositionTypeId
    ? docRef.update(compositionType)
      .then(() => dispatch(receive()))
      .catch(err => {
        dispatch(setCompositionTypes(previousCompositionTypes))
        dispatch(setCompositions(previousCompositions))
        return dispatch(error(err))
      })
    : docRef.update({ [previousCompositionTypeId]: firebase.firestore.FieldValue.delete(), ...compositionType })
      .then(() => {
        return compositionsCollectionRef.doc(compositionTypeId).set({ compositions: previousCompositions[previousCompositionTypeId] })
          .then(() => {
            return compositionsCollectionRef.doc(previousCompositionTypeId).delete()
              .then(() => dispatch(receive()))
              .catch(err => { throw err })
          })
          .catch(err => { throw err })
      })
      .catch(err => {
        dispatch(setCompositionTypes(previousCompositionTypes))
        dispatch(setCompositions(previousCompositions))
        return dispatch(error(err))
      })
}
export const deleteCompositionType = id => (dispatch, getState) => {
  const previousCompositionTypes = getState().compositionTypes.compositionTypes
  const previousCompositions = getState().compositions.compositions

  dispatch(request())
  dispatch(removeCompositionType(id))
  dispatch(removeCompositionTypeId(id))
  return docRef.update({ [id]: firebase.firestore.FieldValue.delete() })
    .then(() => {
      return compositionsCollectionRef.doc(id).delete()
        .then(() => {
          return firebase.storage().ref(`/compositions/${id}/`).delete()
            .then(() => dispatch(receive()))
            .catch(() => {})
        })
        .catch(err => { throw err })
    })
    .catch(err => {
      dispatch(setCompositionTypes(previousCompositionTypes))
      dispatch(setCompositions(previousCompositions))
      return dispatch(error(err))
    })
}

