import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { fetchCompositionTypes } from '../../stores/composition-types/compositionTypes.actions'
import { fetchCompositions } from '../../stores/compositions/compositions.actions'
import { Link, NavLink } from 'react-router-dom'
import AddCompositionTypeForm from './addCompositionTypeForm'
import UpdateCompositionsForm from './updateCompositionsForm'

const mapStateToProps = state => ({
  compTypes: state.compositionTypes.compositionTypes,
  compositions: state.compositions.compositions
})
const mapDispatchToProps = dispatch => ({
  onFetchCompTypes: () => dispatch(fetchCompositionTypes()),
  onFetchCompositions: compositionTypeId => dispatch(fetchCompositions(compositionTypeId))
})

class CompositionsAdmin extends Component {
  state = {
    compositionTypeId: ''
  }

  static getDerivedStateFromProps(props, state) {
    const { compositionTypeId } = props.match.params
    if (!compositionTypeId) return { compositionTypeId: '' }
    if (compositionTypeId === 'add') return { compositionTypeId }
    if (compositionTypeId === state.compositionTypeId) return null
    props.onFetchCompositions(compositionTypeId)
    return { compositionTypeId }
  }

  componentDidMount() {
    const { onFetchCompTypes } = this.props
    onFetchCompTypes()
  }

  render() {
    const { compTypes, match } = this.props
    const { compositionTypeId } = this.state
    const compositionIndex = match.params.compositionIndex

    return (
      !!Object.keys(compTypes).length && <Fragment>
        <ul className='nav nav-tabs' role='tablist'>
          { Object.keys(compTypes).filter(id => compTypes[id] !== null).map((typeId, i) => (
            <li key={i} className='nav-item'>
              <NavLink to={`/admin/compositions/${typeId}`} className='nav-link'>
                { compTypes[typeId] }
              </NavLink>
            </li>
          )) }
          <li className='nav-item'>
            <NavLink to='/admin/compositions/add' className='nav-link'>
              <i className='fa fa-plus' />
            </NavLink>
          </li>
        </ul>
        <div className='tab-content mt-3 text-center'>
          { compTypes[compositionTypeId] && <h2 className='c-accent'>{ compTypes[compositionTypeId] }</h2> }
          { compTypes[compositionTypeId] && <Link className='o-50 c-light' to={`/compositions/${compositionTypeId}`}>
            preview <i className='fa fa-external-link'/>
          </Link> }
          { compositionTypeId === 'add'
              ? <AddCompositionTypeForm />
              : compTypes[compositionTypeId] && <UpdateCompositionsForm compositionTypeId={compositionTypeId} compositionIndex={compositionIndex} />
          }
        </div>
      </Fragment>
    )
  }

  isActive = (pathname, route) => {
    console.log(pathname, route)
    return pathname === route
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompositionsAdmin)
