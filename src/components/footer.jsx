import React, { Component } from 'react'

class Footer extends Component {
  render() {
    return (
      <div className='text-center p-2 c-light' style={{ fontSize: '.67rem' }}>
        © 2019 Holt McCarley. All rights reserved.
      </div>
    )
  }
}

export default Footer
