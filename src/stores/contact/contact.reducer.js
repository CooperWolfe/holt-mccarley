import * as Actions from './contact.actions'
import Modules from '../app.modules'

export const initialState = {
  loading: false,
  error: null,
  contact: {}
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function setContact(state, { contact }) {
  return { ...state, contact }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.CONTACT) throw new Error(`Wrong module. Expected: ${Modules.COMPOSITION_TYPES}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.SET_CONTACT: return setContact(state, action)
  default: return state
  }
}
