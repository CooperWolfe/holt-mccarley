import React from 'react'
import { connect } from 'react-redux'
import Form from '../../helpers/form'
import Joi from 'joi-browser'
import { login } from '../../stores/auth/auth.actions'
import history from '../../services/history'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  onLogin: (email, password) => dispatch(login(email, password))
})

class LoginForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      email: '',
      password: ''
    }
  }
  schema = {
    email: Joi.string().email().required().error(() => ({ message: 'This must be a valid email' })),
    password: Joi.string().required().error(() => ({ message: 'Password is required' }))
  }
  errorStyle = { color: 'var(--accent)' }

  render() {
    return (
      <form className='text-center' onSubmit={this.handleSubmit}>
        { this.renderField('Email', 'email', 'email', 'text-center') }
        { this.renderField('Password', 'password', 'password', 'text-center') }
        { this.renderButton('submit', 'outline-success', 'Login') }
      </form>
    )
  }

  doSubmit = () => {
    const { onLogin } = this.props
    const { email, password } = this.state.data

    onLogin(email, password).then(() => history.push('/admin'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
