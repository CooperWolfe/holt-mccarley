import React from 'react'
import Navbar from '../helpers/navbar'
import { fetchCompositionTypes } from '../stores/composition-types/compositionTypes.actions'
import { connect } from 'react-redux'

const mapStateToProps = state => ({
  compTypes: state.compositionTypes.compositionTypes,
  authenticated: state.auth.authenticated
})
const mapDispatchToProps = dispatch => ({
  onFetchCompTypes: () => dispatch(fetchCompositionTypes())
})

const authenticatedLeft = [
  { text: 'Home', url: '/admin', exact: true },
  { text: 'Compositions', url: '/admin/compositions' },
  { text: 'Connect', url: '/admin/connect' }
]
const unauthenticatedLeft = [
  { text: 'Home', url: '/', exact: true },
  { text: 'Compositions', links: [] },
  { text: 'Connect', url: '/connect' }
]
const authenticatedRight = [
  { text: 'Logout', url: '/logout' }
]
const unauthenticatedRight = [
  { text: <i className='fa fa-user-circle' style={{ fontSize: '1.5rem' }} />, url: '/login' }
]

class Appnav extends Navbar {
  state = { ...Navbar.defaultState }

  static getDerivedStateFromProps(props) {
    const { compTypes, authenticated } = props

    const left = authenticated ? authenticatedLeft : unauthenticatedLeft
    const right = authenticated ? authenticatedRight : unauthenticatedRight
    if (authenticated) return { left, right }

    let i = 0
    while (i < left.length && left[i].text !== 'Compositions') ++i

    return {
      left: [
        ...left.slice(0, i),
        {
          text: 'Compositions',
          links: Object.keys(compTypes).map(id => ({
            text: compTypes[id],
            url: authenticated ? `/admin/compositions/${id}` : `/compositions/${id}`
          }))
        },
        ...left.slice(i+1)
      ],
      right
    }
  }

  componentDidMount() {
    const { onFetchCompTypes } = this.props
    onFetchCompTypes()
  }

  renderBrand = () => {
    return <img src='/brand.png' alt='Holt McCarley' width={75} height={75} />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Appnav)
