import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from './app.reducer'
import firebase from '../services/firebase'
import { authenticate, unauthenticate } from './auth/auth.actions'

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
const compose = reduxDevTools && reduxDevTools({})
const middleware = (compose && compose(applyMiddleware(thunkMiddleware))) || applyMiddleware(thunkMiddleware)
const store = createStore(reducer, middleware)

firebase.auth().onAuthStateChanged(user => store.dispatch(user ? authenticate() : unauthenticate()))

export default store
