import React, { Component } from 'react'
import Joi from 'joi-browser'

const defaultState = {
  data: {},
  errors: {}
}

class Form extends Component {
  state = { ...defaultState }
  errorStyle = { color: 'red' }

  validate = () => {
    const opts = { abortEarly: false, allowUnknown: true }
    const result = Joi.validate(this.state.data, this.schema, opts)
    if (!result.error) return null

    const errors = result.error.details.reduce((errs, err) => ({ ...errs, [err.path[0]]: err.message }), {})

    this.setState({ errors })
    return errors
  }
  validateProperty = (key, value) => {
    const obj = { [key]: value }
    const schema = { [key]: this.schema[key] }
    const { error } = Joi.validate(obj, schema)
    return error && error.details[0].message
  }
  handleSubmit = e => {
    e.preventDefault()
    if (this.validate()) return
    if (this.doSubmit) this.doSubmit(e)
  }
  handleChange = e => {
    const input = e.currentTarget
    const error = this.validateProperty(input.name, input.value)
    input.setCustomValidity(error || '')

    const { data } = this.state
    data[input.name] = input.type === 'number' ? +input.value : input.value
    this.setState({ data })
    if (this.doChange) this.doChange(e)
  }

  renderHeader = title => <h2>{ title }</h2>
  renderField = (label, name, type = 'text', className = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name}>{ label }</label>
        <input id={name} name={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} type={type}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderTextArea = (label, name, rows = 3, className = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name}>{ label }</label>
        <textarea name={name} id={name} value={data[name]} onChange={this.handleChange} className={`form-control ${className}`} rows={rows}/>
        { errors[name] && <small style={this.errorStyle}>{ errors[name] }</small> }
      </div>
    )
  }
  renderSelect = (label, name, opts, className = '') => {
    const { data, errors } = this.state
    return (
      <div className='form-group'>
        <label htmlFor={name}>{ label }</label>
        <select name={name} id={name} className={`form-control ${className}`} value={data[name]} onChange={this.handleChange}>
          <option>None</option>
          { opts.options.map((opt, i) => (
            <option key={i} value={(opts.valueKey && opt[opts.valueKey]) || opt}>
              { (opts.labelKey && opt[opts.labelKey]) || (opts.valueKey && opt[opts.valueKey]) || opt }
            </option>
          )) }
        </select>
        { errors[name] && <small style={{ color: 'red' }}>{ errors[name] }</small> }
      </div>
    )
  }
  renderButton = (type, color, label, className = '') => {
    const clazz = `btn btn-${color} ${className}`
    return (
      <button className={clazz} type={type}>{ label }</button>
    )
  }
}

Form.defaultState = defaultState

export default Form
