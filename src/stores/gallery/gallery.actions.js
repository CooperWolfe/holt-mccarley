import Modules from '../app.modules'
import firebase from '../../services/firebase'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const SET_GALLERY = 'SET_GALLERY'
export const SELECT_GALLERY_IMAGE = 'SELECT_GALLERY_IMAGE'
export const DESELECT_GALLERY_IMAGE = 'DESELECT_GALLERY_IMAGE'
export const ADD_GALLERY_IMAGE = 'ADD_GALLERY_IMAGE'
export const UPDATE_GALLERY_IMAGE = 'UPDATE_GALLERY_IMAGE'
export const REMOVE_GALLERY_IMAGE = 'REMOVE_GALLERY_IMAGE'

// Synchronous
export const request = () => ({ module: Modules.GALLERY, type: REQUEST })
export const receive = () => ({ module: Modules.GALLERY, type: RECEIVE })
export const error = err => ({ module: Modules.GALLERY, type: ERROR, err })
export const setGallery = gallery => ({ module: Modules.GALLERY, type: SET_GALLERY, gallery })
export const selectGalleryImage = id => ({
  module: Modules.GALLERY,
  type: SELECT_GALLERY_IMAGE,
  id: typeof id === 'string' ? id : null,
  galleryImage: id
})
export const deselectGalleryImage = () => ({ module: Modules.GALLERY, type: DESELECT_GALLERY_IMAGE })
export const addGalleryImage = galleryImage => ({ module: Modules.GALLERY, type: ADD_GALLERY_IMAGE, galleryImage })
export const updateGalleryImage = (galleryImage, index) => ({ module: Modules.GALLERY, type: UPDATE_GALLERY_IMAGE, galleryImage, index })
export const removeGalleryImage = index => ({ module: Modules.GALLERY, type: REMOVE_GALLERY_IMAGE, index })

// Asynchronous
const docRef = firebase.firestore().collection('site-data').doc('gallery')
export const fetchGallery = () => dispatch => {
  dispatch(request())
  return docRef.get()
    .then(snapshot => snapshot.get('gallery'))
    .then(gallery => dispatch(setGallery(gallery)))
    .then(() => dispatch(receive()))
    .catch(err => dispatch(error(err)))
}
export const storeGalleryImage = galleryImageFile => (dispatch, getState) => {
  dispatch(request())
  const previousGallery = getState().gallery.gallery
  dispatch(addGalleryImage('https://firebasestorage.googleapis.com/v0/b/holt-mccarley.appspot.com/o/loading.gif?alt=media&token=ffad9856-b06f-4d78-af6f-a12e63e858cc'))
  return firebase.storage().ref(`gallery/${galleryImageFile.name}`).put(galleryImageFile)
    .then(snapshot => snapshot.ref.getDownloadURL()
      .then(url => {
        return docRef.update({ gallery: firebase.firestore.FieldValue.arrayUnion(url) })
          .then(() => dispatch(updateGalleryImage(url, previousGallery.length)))
          .then(() => dispatch(receive()))
          .catch(err => { throw err })
      })
      .catch(err => { throw err })
    )
    .catch(err => {
      dispatch(setGallery(previousGallery))
      return dispatch(error(err))
    })
}
export const deleteGalleryImage = index => (dispatch, getState) => {
  dispatch(request())
  const previousGallery = getState().gallery.gallery
  dispatch(removeGalleryImage(index))
  return docRef.update({ gallery: firebase.firestore.FieldValue.arrayRemove(previousGallery[index]) })
    .then(() => {
      return firebase.storage().refFromURL(previousGallery[index]).delete()
        .then(() => dispatch(receive()))
        .catch(err => { throw err })
    })
    .catch(err => {
      dispatch(setGallery(previousGallery))
      return dispatch(error(err))
    })
}
