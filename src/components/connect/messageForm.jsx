import React from 'react'
import Form from '../../helpers/form'
import Joi from 'joi-browser'

class MessageForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      name: '',
      email: '',
      subject: '',
      message: ''
    }
  }
  schema = {
    name: Joi.string().required().error(() => ({ message: 'Name cannot be empty' })),
    email: Joi.string().email().required().error(() => ({ message: 'Email must be a valid email' })),
    subject: Joi.string().required().error(() => ({ message: 'Subject cannot be empty' })),
    message: Joi.string().required().error(() => ({ message: 'Message cannot be empty' }))
  }
  errorStyle = { color: 'var(--accent)', textShadow: '2px 2px 4px black' }

  render() {
    const { className, style } = this.props
    return (
      <form className={className} style={style} onSubmit={this.handleSubmit}>
        { this.renderField('Name', 'name') }
        { this.renderField('Email', 'email') }
        { this.renderField('Subject', 'subject') }
        { this.renderTextArea('Message', 'message') }
        { this.renderButton('submit', 'outline-success', 'Send') }
      </form>
    )
  }
}

export default MessageForm
