import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  imageSrc: PropTypes.string.isRequired,
  imageAlt: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.node,
  borderRadius: PropTypes.string,
  objectFit: PropTypes.string
}
const defaultProps = {
  style: {},
  className: '',
  borderRadius: '0',
  objectFit: 'cover'
}

class Image extends Component {
  render() {
    const { style, className, imageSrc, children, imageAlt, borderRadius, objectFit } = this.props

    return (
      <div className={`position-relative ${className}`} style={style}>
        <img className='w-100 h-100 position-absolute' src={imageSrc} alt={imageAlt} style={{ objectFit, borderRadius }} />
        <div className='o-25 o-hover-100 transition container position-absolute h-100'>
          { children }
        </div>
      </div>
    )
  }
}

Image.propTypes = propTypes
Image.defaultProps = defaultProps

export default Image
