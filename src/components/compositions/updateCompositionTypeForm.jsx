import React from 'react'
import { connect } from 'react-redux'
import Form from '../../helpers/form'
import PropTypes from 'prop-types'
import { postCompositionType } from '../../stores/composition-types/compositionTypes.actions'
import Joi from 'joi-browser'
import history from '../../services/history'
import logService from '../../services/log'
import Loader from '../loader'

const mapStateToProps = state => ({
  compTypes: state.compositionTypes.compositionTypes,
  loading: state.compositionTypes.loading
})
const mapDispatchToProps = dispatch => ({
  onUpdateCompositionType: (compositionType, previousCompositionTypeId) => dispatch(postCompositionType(compositionType, previousCompositionTypeId))
})

const propTypes = {
  compositionTypeId: PropTypes.string.isRequired
}

class UpdateCompositionTypeForm extends Form {
  state = {
    ...Form.defaultState,
    compositionTypeId: '',
    data: {
      id: this.props.compositionTypeId,
      label: this.props.compTypes[this.props.compositionTypeId]
    }
  }
  schema = {
    id: Joi.string().required().error(() => ({ message: 'ID cannot be empty' })),
    label: Joi.string().required().error(() => ({ message: 'Label cannot be empty' }))
  }

  static getDerivedStateFromProps(props, state) {
    if (props.compositionTypeId === state.compositionTypeId) return null
    return {
      data: {
        id: props.compositionTypeId,
        label: props.compTypes[props.compositionTypeId]
      },
      compositionTypeId: props.compositionTypeId
    }
  }

  render() {
    const { className, loading } = this.props
    return (
      <form onSubmit={this.handleSubmit} className={className}>
        { this.renderField('ID', 'id', 'text', 'text-center') }
        { this.renderField('Label', 'label', 'text', 'text-center') }
        { this.renderButton('submit', 'outline-success', loading ? <Loader /> : 'Update') }
      </form>
    )
  }

  doSubmit = () => {
    const { onUpdateCompositionType, compositionTypeId } = this.props
    const { id, label } = this.state.data
    onUpdateCompositionType({ [id]: label }, compositionTypeId).then(() => {
      if (compositionTypeId !== id) history.push(`/admin/compositions`)
      logService.alert('Successfully updated composition type')
    }).catch(err => logService.alertError(err.message))
  }
}

UpdateCompositionTypeForm.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCompositionTypeForm)
