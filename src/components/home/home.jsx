import React, { Component } from 'react'
import { Carousel } from 'react-responsive-carousel'
import '../../../node_modules/react-responsive-carousel/lib/styles/carousel.min.css'
import { fetchGallery } from '../../stores/gallery/gallery.actions'
import { connect } from 'react-redux'
import { fetchBio } from '../../stores/bio/bio.actions'
import { parse } from 'markdown'

const mapStateToProps = state => ({
  imageUrls: state.gallery.gallery,
  bio: state.bio.bio
})
const mapDispatchToProps = dispatch => ({
  onFetchGallery: () => dispatch(fetchGallery()),
  onFetchBio: () => dispatch(fetchBio())
})

class Home extends Component {
  componentDidMount() {
    const { onFetchGallery, onFetchBio } = this.props
    onFetchGallery()
    onFetchBio()
  }

  renderCarousel = () => {
    const { imageUrls } = this.props
    try {
      return (
        <Carousel>
          {imageUrls.map((url, i) => (
            <img key={i} src={url}
                 style={{ maxHeight: 400, objectFit: 'contain' }}
                 alt="Holt McCarley's gallery"/>
          ))}
        </Carousel>
      )
    } catch (err) {
      return null
    }
  }
  render() {
    const { bio } = this.props

    return (
      <React.Fragment>
        { this.renderCarousel() }
        <h1 className='c-accent'>Thomas "Holt" McCarley</h1>
        <div className='c-light' dangerouslySetInnerHTML={{ __html: parse(bio) }}/>
      </React.Fragment>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
