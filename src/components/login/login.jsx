import React, { Component } from 'react'
import LoginForm from './loginForm'

class Login extends Component {
  state = {}

  render() {
    return (
      <div className='col-md-6 m-auto'>
        <LoginForm />
      </div>
    )
  }
}

export default Login
