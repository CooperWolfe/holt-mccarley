import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Form from '../../helpers/form'
import Joi from 'joi-browser'
import { Link } from 'react-router-dom'
import { storeComposition } from '../../stores/compositions/compositions.actions'
import PropTypes from 'prop-types'
import history from '../../services/history'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  onAddComposition: (composition, compositionTypeId) => dispatch(storeComposition(composition, compositionTypeId))
})

const propTypes = {
  compositionTypeId: PropTypes.string.isRequired
}

class AddCompositionForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      title: '',
      year: new Date().getFullYear(),
      description: '',
      files: []
    }
  }
  schema = {
    title: Joi.string().required().error(() => ({ message: 'Title cannot be empty' })),
    year: Joi.number().required().error(() => ({ message: 'Year cannot be empty' })),
    description: Joi.string().optional()
  }

  render() {
    const { className, compositionTypeId } = this.props

    return <Fragment>
      <div className='text-left'>
        <Link className='c-accent' to={`/admin/compositions/${compositionTypeId}`}><i className='fa fa-angle-left'/> Back</Link>
      </div>
      <h2 className='c-accent'>Add Composition</h2>
      <form onSubmit={this.handleSubmit} className={className}>
        { this.renderField('Title', 'title', 'text', 'text-center') }
        { this.renderField('Year', 'year', 'number', 'text-center pl-4') }
        { this.renderTextArea('Description', 'description', 2) }
        { this.renderButton('submit', 'outline-success', 'Save') }
      </form>
    </Fragment>
  }

  doSubmit = () => {
    const { onAddComposition, compositionTypeId } = this.props
    const { data } = this.state
    onAddComposition(data, compositionTypeId).then(() => {
      history.push(`/admin/compositions/${compositionTypeId}`)
    })
  }
}

AddCompositionForm.propTypes = propTypes

export default connect(mapStateToProps, mapDispatchToProps)(AddCompositionForm)
