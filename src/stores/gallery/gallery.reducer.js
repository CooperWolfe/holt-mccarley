import * as Actions from './gallery.actions'
import Modules from '../app.modules'

export const initialState = {
  gallery: ['https://firebasestorage.googleapis.com/v0/b/holt-mccarley.appspot.com/o/loading.gif?alt=media&token=ffad9856-b06f-4d78-af6f-a12e63e858cc'],
  selected: null,
  loading: false,
  error: null
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function setGallery(state, { gallery }) {
  return { ...state, gallery }
}
function selectGalleryImage(state, { id, galleryImage }) {
  if (!id) return { ...state, selected: galleryImage }
  const selected = state.gallery.filter(galleryImage => galleryImage.id === id)[0]
  return { ...state, selected }
}
function deselectGalleryImage(state) {
  return { ...state, selected: null }
}
function addGalleryImage(state, { galleryImage }) {
  return {
    ...state,
    gallery: [ ...state.gallery, galleryImage ]
  }
}
function updateGalleryImage(state, { galleryImage, index }) {
  return {
    ...state,
    gallery: [
      ...state.gallery.slice(0, index),
      galleryImage,
      ...state.gallery.slice(index + 1)
    ]
  }
}
function removeGalleryImage(state, { index }) {
  return {
    ...state,
    gallery: [
      ...state.gallery.slice(0, index),
      ...state.gallery.slice(index + 1)
    ]
  }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.GALLERY) throw new Error(`Wrong module. Expected: ${Modules.GALLERY}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.SET_GALLERY: return setGallery(state, action)
  case Actions.SELECT_GALLERY_IMAGE: return selectGalleryImage(state, action)
  case Actions.DESELECT_GALLERY_IMAGE: return deselectGalleryImage(state)
  case Actions.ADD_GALLERY_IMAGE: return addGalleryImage(state, action)
  case Actions.UPDATE_GALLERY_IMAGE: return updateGalleryImage(state, action)
  case Actions.REMOVE_GALLERY_IMAGE: return removeGalleryImage(state, action)
  default: return state
  }
}
