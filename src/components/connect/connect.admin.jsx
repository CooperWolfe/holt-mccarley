import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import ConnectForm from './connectForm'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({})

class ConnectAdmin extends Component {
  state = {}

  render() {
    return (
      <Fragment>
        <h2 className='text-center c-accent'>Contact Info</h2>
        <Link to='/connect' className='o-50 text-center d-block c-light'>preview <i className='fa fa-external-link'/></Link>
        <ConnectForm />
      </Fragment>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectAdmin)
