import { toast } from 'react-toastify'
// import * as Sentry from '@sentry/browser'

const annoyOptions = {
  hideProgressBar: true,
  draggable: false,
  autoClose: false,
  closeButton: false
}
// Sentry.init({ dsn: "https://689c2f4ba9284761b13ae4835b40cc46@sentry.io/1308323" })

const logService = {
  log: console.log,
  alert(message, onClose) {
    toast(message, { onClose })
  },
  annoy(message, onClose) {
    // noinspection JSCheckFunctionSignatures
    toast.warn(message, { ...annoyOptions, onClose })
  },
  alertError(message, onClose) {
    toast.error(message, { onClose })
  }
  // logSentry(err) {
  //   Sentry.captureException(err)
  // }
}

export default logService
