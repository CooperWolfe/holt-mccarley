import * as Actions from './auth.actions'
import Modules from '../app.modules'
import firebase from '../../services/firebase'

export const initialState = {
  loading: false,
  error: null,
  authenticated: !!firebase.auth().currentUser
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function authenticate(state) {
  return { ...state, authenticated: true }
}
function unauthenticate(state) {
  return { ...state, authenticated: false }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.AUTH) throw new Error(`Wrong module. Expected: ${Modules.AUTH}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.AUTHENTICATE: return authenticate(state)
  case Actions.UNAUTHENTICATE: return unauthenticate(state)
  default: return state
  }
}
