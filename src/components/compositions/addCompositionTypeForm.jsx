import React from 'react'
import { connect } from 'react-redux'
import { storeCompositionType } from '../../stores/composition-types/compositionTypes.actions'
import Form from '../../helpers/form'
import Joi from 'joi-browser'
import logService from '../../services/log'

const mapStateToProps = state => ({})
const mapDispatchToProps = dispatch => ({
  onAddCompositionType: compositionType => dispatch(storeCompositionType(compositionType))
})

class AddCompositionTypeForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      id: '',
      label: ''
    }
  }
  schema = {
    id: Joi.string().required().error(() => ({ message: 'ID cannot be empty' })),
    label: Joi.string().required().error(() => ({ message: 'Label cannot be empty' }))
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className='col-md-6 m-auto text-center'>
        { this.renderField(<span>ID<br/><code>for the url: page will be located at /compositions/_______</code></span>, 'id', 'text', 'text-center') }
        { this.renderField(<span>Label<br/><code>this will appear in the navigation bar</code></span>, 'label', 'text', 'text-center') }
        { this.renderButton('submit', 'outline-success', 'Add') }
      </form>
    )
  }

  doSubmit = () => {
    const { onAddCompositionType } = this.props
    const { id, label } = this.state.data

    onAddCompositionType({ [id]: label })
      .catch(err => logService.alertError(err.message))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCompositionTypeForm)
