import firebase from 'firebase/app'

import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

const config = {
  apiKey: "AIzaSyDqpwjLJIOGm0uuk3UjcbX9p2HwI1eIx9g",
  authDomain: "holt-mccarley.firebaseapp.com",
  databaseURL: "https://holt-mccarley.firebaseio.com",
  projectId: "holt-mccarley",
  storageBucket: "holt-mccarley.appspot.com",
  messagingSenderId: "362251209080"
};
firebase.initializeApp(config)
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase
