import * as Actions from './compositions.actions'
import Modules from '../app.modules'

export const initialState = {
  loading: false,
  error: null,
  compositions: {}
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function setCompositions(state, { compositions, compositionTypeId }) {
  if (compositions === undefined) return state
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: compositions
    }
  }
}
function setCompositionTypeIds(state, { compositionTypeIds }) {
  const compositions = {}
  for (let i = 0; i < compositionTypeIds.length; ++i) {
    compositions[compositionTypeIds[i]] = []
  }

  return {
    ...state,
    compositions: {
      ...compositions,
      ...state.compositions
    }
  }
}
function updateCompositionTypeId(state, { previousCompositionTypeId, compositionTypeId }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [previousCompositionTypeId]: null,
      [compositionTypeId]: state.compositions[previousCompositionTypeId]
    }
  }
}
function addComposition(state, { composition, compositionTypeId }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: [
        ...state.compositions[compositionTypeId],
        composition
      ]
    }
  }
}
function removeCompositionTypeId(state, { compositionTypeId }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: null
    }
  }
}
function removeCompositionFile(state, { compositionTypeId, compositionIndex, fileIndex }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: [
        ...state.compositions[compositionTypeId].slice(0, compositionIndex),
        {
          ...state.compositions[compositionTypeId][compositionIndex],
          files: [
            ...state.compositions[compositionTypeId][compositionIndex].files.slice(0, fileIndex),
            ...state.compositions[compositionTypeId][compositionIndex].files.slice(fileIndex + 1)
          ]
        },
        ...state.compositions[compositionTypeId].slice(compositionIndex + 1)
      ]
    }
  }
}
function removeComposition(state, { compositionTypeId, compositionIndex }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: [
        ...state.compositions[compositionTypeId].slice(0, compositionIndex),
        ...state.compositions[compositionTypeId].slice(compositionIndex + 1)
      ]
    }
  }
}
function updateComposition(state, { compositionTypeId, compositionIndex, composition }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: [
        ...state.compositions[compositionTypeId].slice(0, compositionIndex),
        {
          ...state.compositions[compositionTypeId][compositionIndex],
          ...composition
        },
        ...state.compositions[compositionTypeId].slice(compositionIndex + 1)
      ]
    }
  }
}
function addCompositionFile(state, { compositionTypeId, compositionIndex, file }) {
  return {
    ...state,
    compositions: {
      ...state.compositions,
      [compositionTypeId]: [
        ...state.compositions[compositionTypeId].slice(0, compositionIndex),
        {
          ...state.compositions[compositionTypeId][compositionIndex],
          files: [
            ...state.compositions[compositionTypeId][compositionIndex].files,
            file
          ]
        },
        ...state.compositions[compositionTypeId].slice(compositionIndex + 1)
      ]
    }
  }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.COMPOSITIONS) throw new Error(`Wrong module. Expected: ${Modules.COMPOSITIONS}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.SET_COMPOSITIONS: return setCompositions(state, action)
  case Actions.SET_COMPOSITION_TYPE_IDS: return setCompositionTypeIds(state, action)
  case Actions.UPDATE_COMPOSITION_TYPE_ID: return updateCompositionTypeId(state, action)
  case Actions.ADD_COMPOSITION: return addComposition(state, action)
  case Actions.REMOVE_COMPOSITION_TYPE_ID: return removeCompositionTypeId(state, action)
  case Actions.REMOVE_COMPOSITION_FILE: return removeCompositionFile(state, action)
  case Actions.REMOVE_COMPOSITION: return removeComposition(state, action)
  case Actions.UPDATE_COMPOSITION: return updateComposition(state, action)
  case Actions.ADD_COMPOSITION_FILE: return addCompositionFile(state, action)
  default: return state
  }
}
