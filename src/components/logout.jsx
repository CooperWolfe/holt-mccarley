import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { logout } from '../stores/auth/auth.actions'
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(logout())
})

class Logout extends Component {
  state = {}

  componentDidMount() {
    const { onLogout } = this.props
    onLogout()
  }

  render() {
    const { match } = this.props

    return <Redirect from={match.url} to='/' />
  }
}

export default connect(null, mapDispatchToProps)(Logout)
