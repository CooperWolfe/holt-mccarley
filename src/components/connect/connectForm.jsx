import React from 'react'
import { connect } from 'react-redux'
import Form from '../../helpers/form'
import { fetchContact, storeContact } from '../../stores/contact/contact.actions'
import Joi from 'joi-browser'
import logService from '../../services/log'
import Loader from '../loader'

const mapStateToProps = state => ({
  contactInfo: state.contact.contact,
  loading: state.contact.loading
})
const mapDispatchToProps = dispatch => ({
  onFetchContact: () => dispatch(fetchContact()),
  onUpdateContact: contactInfo => dispatch(storeContact(contactInfo))
})

class ConnectForm extends Form {
  state = {
    ...Form.defaultState,
    data: {
      email: '',
      phone: '',
      facebookUrl: '',
      instagramUrl: '',
      youtubeUrl: ''
    },
    contactFetched: false
  }
  schema = {
    email: Joi.string().email().required().error(() => ({ message: 'Email must be a valid email' })),
    phone: Joi.string().required().error(() => ({ message: 'Phone cannot be empty' })),
    facebookUrl: Joi.string().required().error(() => ({ message: 'Facebook URL cannot be empty' })),
    instagramUrl: Joi.string().required().error(() => ({ message: 'Instagram URL cannot be empty' })),
    youtubeUrl: Joi.string().required().error(() => ({ message: 'YouTube URL cannot be empty' }))
  }

  static getDerivedStateFromProps(props, state) {
    if (state.contactFetched) return null;
    if (!Object.keys(props.contactInfo).length) return null;
    return { data: props.contactInfo, contactFetched: true }
  }

  componentDidMount() {
    const { onFetchContact } = this.props
    onFetchContact()
  }

  render() {
    const { loading } = this.props

    return (
      <form className='col-lg-8 ml-auto mr-auto mt-3 text-center' onSubmit={this.handleSubmit}>
        { this.renderField('Email', 'email', 'text', 'text-center') }
        { this.renderField('Phone', 'phone', 'text', 'text-center') }
        { this.renderField('Facebook', 'facebookUrl', 'text', 'text-center') }
        { this.renderField('Instagram', 'instagramUrl', 'text', 'text-center') }
        { this.renderField('YouTube', 'youtubeUrl', 'text', 'text-center') }
        { this.renderButton('submit', 'outline-success', loading ? <Loader /> : 'Update') }
      </form>
    )
  }

  doSubmit = () => {
    const { onUpdateContact } = this.props
    const { data } = this.state
    onUpdateContact(data).then(() => logService.alert('Successfully updated contact info'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectForm)
