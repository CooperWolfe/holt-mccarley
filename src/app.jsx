import React, { Component } from 'react'
import Header from './components/header'
import AppNav from './components/appnav'
import Content from './components/content'
import Footer from './components/footer'
import { ToastContainer } from 'react-toastify'

class App extends Component {
  render() {
    return (
      <div className='app'>
        <ToastContainer position='top-center'/>
        <Header src='https://firebasestorage.googleapis.com/v0/b/holt-mccarley.appspot.com/o/cover.jpg?alt=media&token=98c95c4c-d936-46ee-a46a-c511af412b87'
                alt='Holt McCarley sitting in a chair'
                textStyle={{ fontFamily: 'var(--header)', color: 'var(--light)' }}>
          Holt McCarley
          <br/>
          <p className='c-accent ff-text' style={{ fontSize: '2rem' }}>Composer</p>
        </Header>
        <AppNav breakpoint='sm' style={{ backgroundImage: 'linear-gradient(#002645, var(--primary))' }} />
        <Content />
        <Footer />
      </div>
    )
  }
}

export default App
