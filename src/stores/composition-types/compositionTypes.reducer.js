import * as Actions from './compositionTypes.actions'
import Modules from '../app.modules'

export const initialState = {
  compositionTypes: {},
  selected: null,
  loading: false,
  error: null
}

function request(state) {
  return { ...state, loading: true }
}
function receive(state) {
  return { ...state, loading: false, error: null }
}
function error(state, { err: error }) {
  return { ...state, loading: false, error }
}
function setCompositionTypes(state, { compositionTypes }) {
  return { ...state, compositionTypes }
}
function selectCompositionType(state, { id, compositionType }) {
  if (!id) return { ...state, selected: compositionType }
  const selected = state.compositionTypes.filter(compositionType => compositionType.id === id)[0]
  return { ...state, selected }
}
function deselectCompositionType(state) {
  return { ...state, selected: null }
}
function addCompositionType(state, { compositionType }) {
  return {
    ...state,
    compositionTypes: { ...state.compositionTypes, ...compositionType }
  }
}
function updateCompositionType(state, { compositionType, previousCompositionTypeId }) {
  return {
    ...state,
    compositionTypes: {
      ...state.compositionTypes,
      [previousCompositionTypeId]: null,
      ...compositionType
    }
  }
}
function removeCompositionType(state, { id }) {
  return {
    ...state,
    compositionTypes: {
      ...state.compositionTypes,
      [id]: null
    }
  }
}

export function reducer(state = initialState, action) {
  if (action.module !== Modules.COMPOSITION_TYPES) throw new Error(`Wrong module. Expected: ${Modules.COMPOSITION_TYPES}, Actual: ${action.module}`)
  switch (action.type) {
  case Actions.REQUEST: return request(state)
  case Actions.RECEIVE: return receive(state)
  case Actions.ERROR: return error(state, action)
  case Actions.SET_COMPOSITION_TYPES: return setCompositionTypes(state, action)
  case Actions.SELECT_COMPOSITION_TYPE: return selectCompositionType(state, action)
  case Actions.DESELECT_COMPOSITION_TYPE: return deselectCompositionType(state)
  case Actions.ADD_COMPOSITION_TYPE: return addCompositionType(state, action)
  case Actions.UPDATE_COMPOSITION_TYPE: return updateCompositionType(state, action)
  case Actions.REMOVE_COMPOSITION_TYPE: return removeCompositionType(state, action)
  default: return state
  }
}

