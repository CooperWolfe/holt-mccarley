import Modules from '../app.modules'
import firebase from '../../services/firebase'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const SET_BIO = 'SET_BIO'

// Synchronous
export const request = () => ({
  module: Modules.BIO,
  type: REQUEST
})
export const receive = () => ({
  module: Modules.BIO,
  type: RECEIVE
})
export const error = err => ({
  module: Modules.BIO,
  type: ERROR,
  err
})
export const setBio = bio => ({
  module: Modules.BIO,
  type: SET_BIO,
  bio
})

// Asynchronous
const docRef = firebase.firestore().collection('site-data').doc('bio')
export const fetchBio = () => dispatch => {
  dispatch(request())
  return docRef.get()
    .then(snapshot => snapshot.get('html'))
    .then(html => dispatch(setBio(html)))
    .then(() => dispatch(receive()))
    .catch(err => dispatch(error(err)))
}
export const storeBio = bio => (dispatch, getState) => {
  dispatch(request())
  const previousBio = getState().bio.bio
  dispatch(setBio(bio))
  return docRef.update({ html: bio })
    .then(() => dispatch(receive()))
    .catch(err => {
      dispatch(setBio(previousBio))
      return dispatch(error(err))
    })
}
