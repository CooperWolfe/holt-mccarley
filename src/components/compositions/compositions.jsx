import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCompositions } from '../../stores/compositions/compositions.actions'
import CompositionCard from './compositionCard'

const mapStateToProps = state => ({
  compositions: state.compositions.compositions,
  compTypes: state.compositionTypes.compositionTypes
})
const mapDispatchToProps = dispatch => ({
  onFetchCompositions: compositionTypeId => dispatch(fetchCompositions(compositionTypeId))
})

class Compositions extends Component {
  state = {
    compositionTypeId: ''
  }

  static getDerivedStateFromProps(props, state) {
    const { compositionTypeId } = props.match.params
    if (compositionTypeId === state.compositionTypeId) return null
    props.onFetchCompositions(compositionTypeId)
    return { compositionTypeId }
  }

  render() {
    const { compositionTypeId } = this.state
    const { compTypes } = this.props
    let { compositions } = this.props

    compositions = compositions[compositionTypeId]

    return (
      <div>
        <h1 className='c-accent text-center'>{ compTypes[compositionTypeId] }</h1>
        { compositions && (
          <div className='row'>
            { compositions.map((composition, i) => (
              <div key={i} className='col-12 col-sm-6 col-lg-4 mt-3'>
                <CompositionCard composition={composition}/>
              </div>
            )) }
          </div>
        ) }
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Compositions)
