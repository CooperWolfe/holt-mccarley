import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  composition: PropTypes.shape({
    title: PropTypes.string.isRequired,
    year: PropTypes.number.isRequired,
    description: PropTypes.string,
    files: PropTypes.arrayOf(PropTypes.shape({
      description: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired
    }))
  }).isRequired,
  onDelete: PropTypes.func
}

class CompositionCard extends Component {
  render() {
    const { composition, onDelete } = this.props

    return (
      <div className='card bg-light c-primary'>
        <div className='card-body c-primary'>
          <h5 className='card-title'>"{ composition.title }"</h5>
          <h6 className='card-subtitle mb-2 text-muted'>{ composition.year }</h6>
          <p className='card-text'>{ composition.description }</p>
        </div>
        { composition.files && (
          <ul className='list-group list-group-flush'>
            { composition.files.map((file, i) => (
              <li key={i} className='list-group-item bg-accent'>
                <a onClick={e => e.stopPropagation()} className='c-primary' href={file.url}>{ file.description } <i className='fa fa-external-link'/></a>
              </li>
            )) }
          </ul>
        ) }
        { !!onDelete && (
          <div className='card-body'>
            <button className='btn btn-outline-danger w-100' onClick={onDelete}><i className='fa fa-trash'/></button>
          </div>
        ) }
      </div>
    )
  }
}

CompositionCard.propTypes = propTypes

export default CompositionCard
