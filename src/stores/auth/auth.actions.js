import Modules from '../app.modules'
import firebase from '../../services/firebase'
import logService from '../../services/log'

// Constants
export const REQUEST = 'REQUEST'
export const RECEIVE = 'RECEIVE'
export const ERROR = 'ERROR'
export const AUTHENTICATE = 'AUTHENTICATE'
export const UNAUTHENTICATE = 'UNAUTHENTICATE'

// Synchronous
export const request = () => ({
  module: Modules.AUTH,
  type: REQUEST
})
export const receive = () => ({
  module: Modules.AUTH,
  type: RECEIVE
})
export const error = err => ({
  module: Modules.AUTH,
  type: ERROR,
  err
})
export const authenticate = () => ({
  module: Modules.AUTH,
  type: AUTHENTICATE
})
export const unauthenticate = () => ({
  module: Modules.AUTH,
  type: UNAUTHENTICATE
})

// Asynchronous
export const login = (email, password) => dispatch => {
  dispatch(request())
  return firebase.auth().signInWithEmailAndPassword(email, password)
    .then(() => dispatch(receive()))
    .catch(err => {
      if (err.code === 'auth/user-not-found')
        logService.alertError('Login failed')
      dispatch(error(err))
    })
}
export const logout = () => dispatch => {
  dispatch(request())
  return firebase.auth().signOut()
    .then(() => dispatch(receive()))
    .catch(err => dispatch(error(err)))
}
