import React, { Component, Fragment } from 'react'
import Image from '../image'
import MessageForm from './messageForm'
import { fetchContact } from '../../stores/contact/contact.actions'
import { connect } from 'react-redux'

const mapStateToProps = state => ({
  contact: state.contact.contact
})
const mapDispatchToProps = dispatch => ({
  onFetchContact: () => dispatch(fetchContact())
})

class Connect extends Component {
  state = {
    imageUrl: 'https://firebasestorage.googleapis.com/v0/b/holt-mccarley.appspot.com/o/contact%2Fhands.jpg?alt=media&token=ff62613a-0526-4f46-80b1-95b7690558d8'
  }

  componentDidMount() {
    const { onFetchContact } = this.props
    onFetchContact()
  }

  renderInfo = (icon, url, label) => {
    return <li style={{ fontSize: '1.25rem', marginTop: '1.25rem' }}>
      <a className='c-light w-100' href={url}>
        <i className={`col-1 fa fa-${icon}`}/>
        <span className='col-11 m-auto'>{ label }</span>
      </a>
    </li>
  }
  render() {
    const { imageUrl } = this.state
    const { email, phone, facebookUrl, instagramUrl, youtubeUrl } = this.props.contact

    return <Fragment>
      <h1 className='text-center c-accent'>Holt McCarley</h1>
      <div className='row'>
        <div className='col-md-6'>
          <Image imageSrc={imageUrl} imageAlt='Send Holt McCarley a message directly' style={{ height: 550 }} borderRadius='1rem'>
            <MessageForm className='p-2'/>
          </Image>
        </div>
        <div className='col-md-6'>
          <ul style={{ listStyleType: 'none' }}>
            { this.renderInfo('envelope', `mailto:${email}`, email) }
            { this.renderInfo('phone', `tel:${phone}`, phone) }
            { this.renderInfo('facebook-square', facebookUrl, 'Like') }
            { this.renderInfo('instagram', instagramUrl, 'Follow') }
            { this.renderInfo('youtube-play', youtubeUrl, 'Subscribe') }
          </ul>
        </div>
      </div>
    </Fragment>
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Connect)
